﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Model;

namespace Shoppinglist.Controllers
{
    [Produces("application/json")]
    [Route("api/Order")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly ShoppingListContext _shoppingListContext;

        public OrderController(ShoppingListContext shoppingListContext)
        {
            _shoppingListContext = shoppingListContext;
        }

        [HttpGet("")]
        public ActionResult<List<Order>> GetAll()
        {
            return _shoppingListContext.Orders
                .Include(o => o.Components)
                .ToList();
        }

        [HttpGet("{id}", Name = "GetOrder")]
        [ProducesResponseType(404)]
        public ActionResult<Order> GetById(int id)
        {
            var order = _shoppingListContext.Orders
                .Include(o => o.Components)
                .Where(o => o.ID == id)
                .SingleOrDefault();

            if (order == null)
                return NotFound();

            return order;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<Order> Create([FromBody]Order order)
        {
            _shoppingListContext.Orders.Add(order);
            _shoppingListContext.SaveChanges();

            return CreatedAtRoute("GetOrder", new { id = order.ID }, order);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(404)]
        public IActionResult Update([FromBody]Order order)
        {
            var dbOrder = _shoppingListContext.Orders
                .Include(o => o.Components)
                .Where(o => o.ID == order.ID)
                .SingleOrDefault();

            if (dbOrder == null)
            {
                return NotFound();
            }

            dbOrder.Servings = order.Servings;

            _shoppingListContext.Components.RemoveRange(dbOrder.Components);

            if (order.Components?.Count > 0)
            {
                order.Components.AddRange(order.Components);
            }

            _shoppingListContext.Orders.Update(dbOrder);
            _shoppingListContext.SaveChanges();

            return NoContent();
        }
    }
}
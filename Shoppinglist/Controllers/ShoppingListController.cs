﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Model;

namespace Shoppinglist.Controllers
{
    [Produces("application/json")]
    [Route("api/ShoppingList")]
    [ApiController]
    public class ShoppingListController : ControllerBase
    {
        private readonly ShoppingListContext _shoppingListContext;

        public ShoppingListController(ShoppingListContext shoppingListContext)
        {
            _shoppingListContext = shoppingListContext;
        }

        [HttpGet("{id}", Name = "GetShoppingList")]
        [ProducesResponseType(404)]
        public ActionResult<ShopList> GetById(int id)
        {
            var order = _shoppingListContext.Orders
                .Include(o => o.Components)
                .Where(o => o.ID == id)
                .SingleOrDefault();

            if (order == null)
                return NotFound();

            var shopList = new ShopList();

            foreach (var orderComponent in order.Components)
            {
                var product = _shoppingListContext
                    .Products
                    .Where(p => p.Name == orderComponent.Name)
                    .OrderBy(p => p.PriceEur / (p.PackageSize ?? 1))
                    .FirstOrDefault();

                if (product == null)
                    continue;

                var requiredQuantity = order.Servings * orderComponent.Quantity;

                var listItem = new ShopListItem
                {
                    Name = product.Name,
                    Brand = product.Brand,
                    PackageSize = product.PackageSize ?? 1,
                    PackagePriceEur = product.PriceEur,
                    SalesUnit = product.SalesUnit,
                    Packages = (int)Math.Ceiling(requiredQuantity / (product.PackageSize ?? 1))
                };

                shopList.Items.Add(listItem);
            }

            return shopList;
        }
    }
}